CREATE SCHEMA IF NOT EXISTS record_of_student_success;
USE record_of_student_success ;

CREATE TABLE IF NOT EXISTS `record_of_student_success`.`address` (
  `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `country` VARCHAR(45) NOT NULL,
  `city` VARCHAR(45) NOT NULL,
  `street` VARCHAR(45) NOT NULL,
  `house_number` INT NOT NULL,
  `flat_number` INT NOT NULL)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `record_of_student_success`.`group` (
  `id` INT NOT NULL auto_increment primary key,
  `code` VARCHAR(45) NOT NULL,
  `specialty` VARCHAR(45) NOT NULL)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `record_of_student_success`.`student` (
  `id` INT NOT NULL auto_increment primary key,
  `name` VARCHAR(45) NOT NULL,
  `photo` LONGBLOB NULL default NULL,
  `autobiographi` VARCHAR(45) NULL default NULL,
  `year_of_entry` INT NOT NULL,
  `birthday` DATE NOT NULL,
  `current_rating` DECIMAL(5,2) NOT NULL,
  `address_id` INT NOT NULL,
  `group_id` INT NOT NULL,
  CONSTRAINT `fk_student_address1`
    FOREIGN KEY (`address_id`)
    REFERENCES `record_of_student_success`.`address` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_group1`
    FOREIGN KEY (`group_id`)
    REFERENCES `record_of_student_success`.`group` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `record_of_student_success`.`subject` (
  `id` INT NOT NULL auto_increment primary key,
  `name` VARCHAR(45) NOT NULL unique)
ENGINE = InnoDB;



CREATE TABLE IF NOT EXISTS `record_of_student_success`.`student_subject` (
  `student_id` INT NOT NULL,
  `subject_id` INT NOT NULL,
  `result_of_module_1` DECIMAL(5,2) NOT NULL,
  `result_of_module_2` DECIMAL(5,2) NOT NULL,
  `grade_in_100_points` DECIMAL(5,2) NOT NULL,
  `grade_in_5_points` DECIMAL(5,2) NOT NULL,
  `teacher` VARCHAR(45) NOT NULL,
  `number_of_semester_for_subject` INT NOT NULL,
  `type_of_control` VARCHAR(6) NOT NULL CHECK (`type_of_control` IN ('credit', 'test')),
  PRIMARY KEY (`grade_in_100_points`, `subject_id`, `student_id`),
  CONSTRAINT `fk_student_subject_subject`
    FOREIGN KEY (`subject_id`)
    REFERENCES `record_of_student_success`.`subject` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_subject_student1`
    FOREIGN KEY (`student_id`)
    REFERENCES `record_of_student_success`.`student` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

INSERT INTO `record_of_student_success`.`address`
VALUES (1, 'Ukraine', 'Lviv', 'Medova Pechera', 39, 804),
	   (3, 'DNR', 'Donetsk', 'Stepana Bandery', 40, 11),
       (2, 'Ukraine', 'Lviv', 'Medova Pechera', 39, 507);


INSERT INTO `record_of_student_success`.`group`
VALUES (1, 'PMA-21', 'System analyle'),
	   (2, 'PMA-22', 'System analyle');


INSERT INTO `record_of_student_success`.`student`
(`id`, `name`, `year_of_entry`, `birthday`, `current_rating`, `address_id`, `group_id`)
VALUES (1, 'Igor', 2017, '1999-11-06', 87.5, 1, 1),
	   (2, 'Sasha', 2017, '2000-07-16', 72, 1, 1),
       (3, 'Misha', 2017, '1999-12-14', 65, 2, 2);

INSERT INTO `record_of_student_success`.`subject`
VALUES (1, 'Matan'),
	   (2, 'Discrete Math'),
       (3, 'Ukrainian History');
       
INSERT INTO `record_of_student_success`.`student_subject`
VALUES (1, 1, 20, 20, 81, 4, 'Andrii', 2, 'test'),
	   (1, 2, 25, 22, 90, 5, 'Kolia', 2, 'test'),
       (1, 3, 15, 20, 74, 4, 'Ihor', 1, 'credit'),
       (2, 1, 14, 20, 71, 4, 'Andrii', 2, 'test'),
	   (2, 2, 15, 12, 61, 3, 'Kolia', 2, 'test'),
       (2, 3, 11, 15, 53, 3, 'Ihor', 1, 'credit'),
       (3, 1, 14, 18, 64, 3, 'Andrii', 2, 'test'),
	   (3, 2, 15, 25, 73, 4, 'Kolia', 2, 'test'),
       (3, 3, 14, 18, 56, 3, 'Ihor', 1, 'credit');