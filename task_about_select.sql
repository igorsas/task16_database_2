-- where, in, any, all

#1
select maker
from product
join pc
on pc.model = product.model
where product.maker
not in (select maker
from product
join laptop
on laptop.model = product.model
group by maker)
group by maker;

-- exist

#2
select maker
from product
where exists
(select * from pc
where speed >= 750
group by model)
group by maker;

-- concat, math, date

#1
select
'середня ціна =', avg(price)
from laptop;

#2
select
concat('model: ', model, ' speed: ', speed, ' ram: ', ram, ' hd: ', hd, ' cd: ', cd, ' price: ', price) as info
from pc;

#3
select
concat(year(date), '.', month(date), '.', dayofmonth(date)) as date
from income;

-- static function and group by

#1
select model, price
from printer
where price in 
(select max(price) from printer);

#2
select product.type, laptop.model, laptop.speed
from laptop
join product
on laptop.model = product.model
where laptop.speed < (select min(pc.speed) from pc);